package cat.itb.testingitb;

import androidx.test.espresso.matcher.ViewMatchers;
import androidx.test.ext.junit.rules.ActivityScenarioRule;
import androidx.test.ext.junit.runners.AndroidJUnit4;

import com.schibsted.spain.barista.assertion.BaristaAssertions;
import com.schibsted.spain.barista.assertion.BaristaVisibilityAssertions;
import com.schibsted.spain.barista.interaction.BaristaClickInteractions;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static androidx.test.espresso.Espresso.onView;
import static androidx.test.espresso.Espresso.pressBack;
import static androidx.test.espresso.action.ViewActions.click;
import static androidx.test.espresso.action.ViewActions.closeSoftKeyboard;

import static androidx.test.espresso.action.ViewActions.typeText;
import static androidx.test.espresso.action.ViewActions.typeTextIntoFocusedView;
import static androidx.test.espresso.assertion.ViewAssertions.doesNotExist;
import static androidx.test.espresso.assertion.ViewAssertions.matches;
import static androidx.test.espresso.matcher.ViewMatchers.isClickable;
import static androidx.test.espresso.matcher.ViewMatchers.isDisplayed;
import static androidx.test.espresso.matcher.ViewMatchers.isEnabled;
import static androidx.test.espresso.matcher.ViewMatchers.withEffectiveVisibility;
import static androidx.test.espresso.matcher.ViewMatchers.withId;
import static androidx.test.espresso.matcher.ViewMatchers.withText;

@RunWith(AndroidJUnit4.class)
public class MainActivityTest {

    public static final String USER_TO_BE_TYPED = "User";
    public static final String PASS_TO_BE_TYPED = "Pass";

    @Rule
    public ActivityScenarioRule<MainActivity> activityRule =
            new ActivityScenarioRule<>(MainActivity.class);

    @Test
    public void elements_are_displayed() {
        onView(withId(R.id.title)).check(matches(isDisplayed()));
        onView(withId(R.id.button)).check(matches(isDisplayed()));
    }

    @Test
    public void elements_have_the_correct_text() {
        onView(withId(R.id.title)).check(matches(withText(R.string.main_activity_title)));
        onView(withId(R.id.button)).check(matches(withText(R.string.next)));
    }

    @Test
    public void nextButton_is_clickable_and_changes_text_to_back_when_clicked() {
        onView(withId(R.id.button))
                .check(matches(isClickable()))
                .perform(click())
                .check(matches(withText(R.string.back)));
    }

    @Test
    public void login_form_behaviour() {
        onView(withId(R.id.editText_username))
                .perform(typeText(USER_TO_BE_TYPED))
                .check(matches(withText(USER_TO_BE_TYPED)))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.editText_password))
                .perform(typeText(PASS_TO_BE_TYPED))
                .check(matches(withText(PASS_TO_BE_TYPED)))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.button))
                .check(matches(isClickable()))
                .perform(click())
                .check(matches(withText(R.string.logged)));
    }

    @Test
    public void when_button_is_clicked_activity_changes_to_second(){
        onView(withId(R.id.button))
                .perform(click());
        onView(withId(R.id.secondActivity))
                .check(matches(isDisplayed()));
    }

    @Test
    public void when_button_is_clicked_activity_changes_to_main(){
        when_button_is_clicked_activity_changes_to_second();
        onView(withId(R.id.secondActivity))
                .check(matches(isDisplayed()));
        onView(withId(R.id.button_second_activity))
                .perform(click());
        onView(withId(R.id.mainActivity))
                .check(matches(isDisplayed()));
    }

    @Test
    public void when_android_back_button_is_clicked_it_goes_to_main_activity(){
        when_button_is_clicked_activity_changes_to_second();
        pressBack();
        onView(withId(R.id.mainActivity))
                .check(matches(isDisplayed()));
    }

    @Test
    public void full_application_test(){
        onView(withId(R.id.editText_username))
                .perform(typeText(USER_TO_BE_TYPED))
                .check(matches(withText(USER_TO_BE_TYPED)))
                .perform(closeSoftKeyboard());
        onView(withId(R.id.editText_password))
                .perform(typeText(PASS_TO_BE_TYPED))
                .check(matches(withText(PASS_TO_BE_TYPED)))
                .perform(closeSoftKeyboard());
        when_button_is_clicked_activity_changes_to_second();
        onView(withId(R.id.title_second_activity))
                .check(matches(withText("Welcome back " + USER_TO_BE_TYPED)));
        onView(withId(R.id.button_second_activity))
                .perform(click());
        onView(withId(R.id.mainActivity))
                .check(matches(isDisplayed()));
        onView(withId(R.id.editText_username))
                .check(matches(withText("")));
        onView(withId(R.id.editText_password))
                .check(matches(withText("")));
    }

    @Test
    public void barista_when_button_is_clicked_activity_changes_to_second(){
        BaristaClickInteractions.clickOn(R.id.button);
        BaristaVisibilityAssertions.assertDisplayed(R.id.secondActivity);
    }
}
